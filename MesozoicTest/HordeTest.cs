﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Horde;

namespace MesozoicTest
{
    [TestClass]
    public class HordeTest
    {
        [TestMethod]
        public void TestAddDino()
        {
            Horde.Horde liste = new Horde.Horde();
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            liste.addDino(louis);
            liste.addDino(nessie);
            Assert.AreEqual(nessie, liste.horde[1]);
            Assert.AreEqual(louis, liste.horde[0]);
            Assert.AreEqual(2, liste.horde.Count);
        }

        [TestMethod]
        public void TestRemoveDino()
        {
            Horde.Horde liste = new Horde.Horde();
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            liste.addDino(louis);
            liste.addDino(nessie);
            Assert.AreEqual(2, liste.horde.Count);
            liste.removeDino(louis);
            Assert.AreEqual(1, liste.horde.Count);
        }

        [TestMethod]
        public void TestEverybodySayHello()
        {
            Horde.Horde liste = new Horde.Horde();
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            liste.addDino(louis);
            liste.addDino(nessie);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n", liste.everybodySayHello());
        }
    }
}
