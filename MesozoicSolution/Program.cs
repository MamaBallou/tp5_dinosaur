﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            Mesozoic.Dinosaur paul = new Mesozoic.Dinosaur("Paul", "representant de l'espece humaine qui n'a rien a faire ici", 18);
            Mesozoic.Dinosaur henri = new Mesozoic.Dinosaur("Henri", "T-rex", 34);

            Horde.Horde dinosaurs = new Horde.Horde();
            dinosaurs.addDino(louis);
            dinosaurs.addDino(nessie);
            dinosaurs.addDino(paul);
            Console.WriteLine(dinosaurs.everybodySayHello());
            dinosaurs.removeDino(louis);
            dinosaurs.addDino(henri);
            Console.WriteLine(dinosaurs.everybodySayHello());
            dinosaurs.removeDino(nessie);
            Console.WriteLine(dinosaurs.everybodySayHello());
            dinosaurs.removeDino(paul);
            Console.WriteLine(dinosaurs.everybodySayHello());
            Console.WriteLine("Et je n'ai plus faim.");

            Console.ReadKey();
        }
    }
}
