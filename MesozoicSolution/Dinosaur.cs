﻿using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        public string name;
        public string specie;
        public int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return "Grrr";
        }

        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, dinosaur.name);
        }

        static string getName(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.name;
        }

        public string getSpecie(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.specie;
        }

        public int getAge(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.age;
        }

        public string setName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            return nom;
        }

        public string setSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            return specie;
        }

        public int setAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            return age;
        }
    }
}